Постепенно собираю чаты и каналы из телеграма, которые так или иначе относятся к игровой индустрии. Условно разделил на профессиональные и геймерские. В первых собраны те каналы и чаты, в которых пользователи объединяются и общаются по какому-либо профессиональному признаку (технология, язык программирования, профессиональная область и т.д.). В вторых собраны площадки по признаку какой-либо игры или игровой студии.

### Оглавление

**Профессиональные**

- [Управление проектами](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D1%83%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0%D0%BC%D0%B8)
- [Маркетинг](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D0%BC%D0%B0%D1%80%D0%BA%D0%B5%D1%82%D0%B8%D0%BD%D0%B3)
- [Game Design](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#game-design)
- [Art](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#art)
- [Animation](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.mdAnimation)
- [Разное](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D1%80%D0%B0%D0%B7%D0%BD%D0%BE%D0%B5)
- [AR/VR](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md?ref_type=heads#arvr)
- [Новости и конференции](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md?ref_type=heads#%D0%BD%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D0%B8-%D0%B8-%D0%BA%D0%BE%D0%BD%D1%84%D0%B5%D1%80%D0%B5%D0%BD%D1%86%D0%B8%D0%B8)
- [Релокация](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md?ref_type=heads#%D1%80%D0%B5%D0%BB%D0%BE%D0%BA%D0%B0%D1%86%D0%B8%D1%8F)
- [Личные блоги (каналы)](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md?ref_type=heads#%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D0%B5-%D0%B1%D0%BB%D0%BE%D0%B3%D0%B8-%D0%BA%D0%B0%D0%BD%D0%B0%D0%BB%D1%8B)
- [Инди разработчики](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D0%B8%D0%BD%D0%B4%D0%B8-%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B8)
- [Unity](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#unity)
- [Работа в rеймдеве](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-%D0%B2-%D0%B3%D0%B5%D0%B9%D0%BC%D0%B4%D0%B5%D0%B2%D0%B5)
- [ПО и ЯП](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md?ref_type=heads#%D0%BF%D0%BE-%D0%B8-%D1%8F%D0%BF)
- [Audio](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#audio)
- [Жанры](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D0%B6%D0%B0%D0%BD%D1%80%D1%8B)

**Геймерские**

- [Dota 2](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#dota-2)
- [Auto](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#auto)
- [Genshin Impact](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#genshin-impact)
- [CS GO](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#cs-go)
- [PUBG](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#pubg)
- [NFT Game](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#nft-game)
- [League of Legends](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#league-of-legends)
- [Mobile Legends](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#mobile-legends)
- [Pokemon GO](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#pokemon-go)
- [Minecraft](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#minecraft)
- [Rush Royale](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#rush-royale)
- [Roblox](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#roblox)
- [Fallout 76](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#fallout-76)
- [War Thunder](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#war-thunder)
- [Supercell Games](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#supercell-games)
- [Raid Shadow](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#raid-shadow)
- [Slot Games](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#slot-games)
- [Xbox](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#xbox)
- [Play Station](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#play-station)
- [Консоли](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D0%BA%D0%BE%D0%BD%D1%81%D0%BE%D0%BB%D0%B8)
- [World of Tanks](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#world-of-tanks)
- [FIFA 22](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#fifa-22)
- [Настольные игры](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D0%BD%D0%B0%D1%81%D1%82%D0%BE%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D0%B8%D0%B3%D1%80%D1%8B)
- [Cyber Games](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#cyber-games)
- [Чаты различных игр](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D1%87%D0%B0%D1%82%D1%8B-%D1%80%D0%B0%D0%B7%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D1%85-%D0%B8%D0%B3%D1%80)
- [Разное](https://gitlab.com/DmitriyBerg/games-chats/-/blob/main/games_chat.md#%D1%80%D0%B0%D0%B7%D0%BD%D0%BE%D0%B5-1)

# Профессиональные чаты

## Управление проектами

- @pmclubvrn - Клуб проектного управления. Площадка менеджеров для знакомства, обмена опытом, обучения и совместного развития среды.
- @agile_ru - Общаемся про Agile, Scrum, Lean, XP, Kanban, инструменты повышения эффективности 
- @atlassian_community - Atlassian community
- @augmoscow - Добро пожаловать в группу Atlassian Community Moscow. Мы проводим регулярные митапы, и нас объединяет интерес к продуктам Atlassian.
- @augspb - Добро пожаловать в чатик Atlassian Community. Мы проводим регулярные митапы, и нас объединяет интерес к продуктам Atlassian.
- @aug_dev - Чат для разработчиков Atlassian
- @ru_notion - Notion.so • Russian Community 
- @projects_jobs - Публикуем вакансии и запросы на поиск работы - управление проектами и рисками, PMBoK, PMP, бюджетированию и т.д.
- @proprocess_ru - Управление проектным бизнесом

## Маркетинг

- @asoprivet - Тут помогают новичкам в ASO.
- @monetizationmediation - Мы обсуждаем нюансы рекламной монетизации мобильных игр и приложений. Чат основан 5 сентября 2019 года. Дискуссии на русском языке.
- @ajaxpublic - Чат по разработке и монетизации мобильных приложений
- @user_acquisition - User Acquisition Chat
- @UA_creatives - новый чат по теме рекламных креативов. Здесь будем обсуждать тренды в рекламе, делиться интересными подходами конкурентов в разных вертикалях.
- @produckty - Продуктовый чат для разработчиков мобильных приложений
- @targetologkontekstolog - Чат создан для общения специалистов - Контекстологов и Таргетологов
- @seochat - SEO chat №1 в Телеграме
- @streamcommunitychat - Для тех, кто делает онлайн-трансляции/стримы. Обсуждение оборудования, камер, коммутации и софта для трансляций (vMix, OBS, Wirecast и тд)
- @asoclub - Чат для обсуждения вебинаров ASOdesk Академии, публикаций в ASO & ASA Channel
- @ppcchat - Контекстная реклама, Директ, AdWords, Ads, Метрика, Analytics
- @compotchat - Сообщества, комьюнити-менеджеры, комьюнити-менеджмент
- @marketagame - О том, как продвигать игры. В основном буду писать про премиум рынок, чуть меньше про мобилки и f2p.

## Game Design

- @gamedesign_chat - Свободное общение про геймдизайн. Вакансии постить можно, если по теме; В идеале - отформатированные и с достаточным количеством информации ;)
- @gamedesgn - GameDesigner chat
- @gamedesignchat - Game Design Chat
- @NoCrunch - Деревня геймдева без кранчей
- https://t.me/+3SnWqWkmMsxkNzUy - Чат гейм-дизайнеров | Манжеты ГД
- @mooshigames_chat - Чат, посвященный основному каналу и группе Mooshi Games
- @thinwallschat - Ламповый чат левел-дизайнеров Thin Walls. Говорим об уровнях, задаем вопросы, делимся интересными ссылками и получаем фидбек.
- @gamedevcoffeeshop - Геймдев флад чатка
- @gamedevporn - Организованное интересное про геймдев. Киберклан. Делитесь с фаертимой.
- @disdoc - Полезные материалы по геймдизайну.
- @justGameDesign - Практика гейм-дизайна
- @justChatGD - Чат Практики ГД
- @nearGD - Гейм-дизайн и рядом - Game-design and Near
- @proleveldesign - Интересное о разработке игровых уровней.

## Art

- @uxchat - uxchat.live - сообщество дизайнеров интерфейсов в Telegram.
- https://t.me/joinchat/IFeejlAvb5q52Ol7YRSjdw - Game UI / UX chat
- @uiux_ru - Обсуждаем темы, посвящённые UI, UX, Design Thinking, best practices и т.д. Идеи, исследования, новости и тренды. Решаем вопросы и вдохновляем друг друга.
- @character_3D - Группа создана для помощи персонажникам. Обсуждаем вопросы по скульптингу, моделированию, текстурированию, ригу, анимации, работе в движках... Вообщем, помогаем друг другу с рабочими вопросами.
- @Art2D - 2D Digital Art
- @chatforartists - Чат для художников
- @ruArtPro - строго для фидбека и общения на тему арта.
- @animefreedom - Anime GALAXY [Чат]
- @uiux_chat - Для всех кто интересуется дизайном UX, UI, Web, продуктовым и тд.
- @parovoz_2021_chat - Анимационная студия «Паровоз» группа
- @quakejourney - Официальный канал Quake2.com.ru. Обсуждаем квейк и все остальное :-)
- @ui_club - Чат для общения Web, UI, UX дизайнеров.
- @chat_art - Художники [Чат]
- @cgsreda - Сообщество специалистов компьютерной графики и 3D разработчиков.
- @CGesus - Делюсь крутейшими 2D и 3D художниками. Иногда публикую туториалы и короткометражные фильмы, а также другие интересные штуки по теме CG.
- @Render_Chat - Официальный чат информационного ресурса RENDER.RU
- @ru_montage - Чат видеомонтажёров.
- @ru_montage_flood - Флудилка
- @MOTIONTALK - Моушн чат. Вопросы по C4D, AE, 3ds Max
- @CGIT_Vines - Канал о сферах CG, Игр и Генератива, все с помощью чего мы генерируем визуальное и интерактивное искусство.
- @uxidesign - UI/UX Designer
- @gameuiux - Ру-канал про игровые интерфейсы, UX для игр и всего, что с ними связано!

## Animation

- @animation_chat - Это чат для 2D и 3D аниматоров: здесь мы делимся работами, помогаем друг-другу, общаемся и главное - делаем все это с удовольствием!
- @Animation_is_Chat - Animation is.... Chat
- @realtimevfx - Real-Time VFX
- @spine_animators - Это чат для 2D Spine аниматоров
- @SpineFlood - Spine Flood
- @editorspro - Видеомонтаж и CG. Везде и на всех платформах. Обсуждения, технические и программные вопросы, обмен опытом.
- https://t.me/joinchat/TW48W-m_IjIsEGIZ - OninationSquad Chat (Персонажная анимация)

## Разное

- @gamedevchat - Чат для людей индустрии gamedev. Для обсуждения ремесла, компаний, проектов, новостей и просто поболтать.
- @network_devs - Разработка онлайн игр
- @legalgd - Геймдев: правовые вопросы
- @zhiza_discuss - Обсуждения подкаста «Жиза ГД»
- @PhysicITGameDev - Physic IT GameDev RU
- https://t.me/joinchat/O58bqYJ6OglSHhwo - Как Делают Игры
- @kojima_talks - Ламповое общение про игровую индустрию.
- https://t.me/joinchat/TdL3jBaORgj03DM6 - Чат про издательства игр в Steam
- @fstormcommunity - Fstorm Community License
- @CarRendering - Добро пожаловать! В данном чате мы обсуждаем авторендеринг и все что с ним связано.
- @xyz_podcast - XYZ Chat
- @qa_gamedev - Говорим о тестировании игр, обмениваемся опытом.
- @narratorika - Сценаристика видеоигр и нарративный дизайн
- @joinmdld - Всё, посвящённое видеоиграм: от гейм и левел-дизайна, до видеоигровых новостей и моих мыслей.
- @gooogamesinfo - Новости, полезные ссылки, вакансии, анонсы и новинки игр. Подписывайтесь, чтобы не пропустить самое важное!
- @moscow_gamedev - чат Московских разработчиков
- @gamedev_moscow - Московский геймдев, он как бы есть, но его как бы нет.
- https://t.me/+AqJiuzkYtBw4MjEy - Геймдев Шрёдингера (чат)
- @gamedevreports - GameDev Reports - by devtodev
- @gamedevstairs - Помогаем получить работу в геймдеве с нуля. Здесь публикуются самые важные объявления, в частности - ссылки на видео.
- @thegamebiz - Games as business by The Games Fund
- @igrotochka - Чат представителей индустрии видеоигр, которые работают или ведут бизнес в России.
- @startgame_rsv - Официальный канал конкурса «Начни игру». Организатор — АНО «Россия — страна возможностей».
- @iti_gameloc - Локализация игр

## AR/VR

- @avracommunity859 - Чат для создающих и интересующихся AR/VR, обмена опытом и новостями. От AVRA Ассоциации AR/VR
- @arkithack5f - чат для общения участников онлайн-конкурса fun-проектов в дополненной реальности, созданных на ARKit Hack и Unity

## Новости и конференции

- @devgamm - DevGAMM
- @wnconf - Обсуждаем все, что касается конференций WN
- @gdt_yerevan - GDTalents Ереван - ламповые встречи
- @gdbalkans - GDTalents Балканы - митапы
- @gdt_tbilisi - GDTalents Тбилиси - ламповые встречи
- @gdt_portugal - GDTalents Португалия - ламповы встречи
- @gamedev_suffering - О разработке игр и новостях индустрии.
- @gamedev_suffering_chating - Gamedev страдалка-болталка
- @hyperduh - Мегамикроиздание о видеоиграх и культуре вокруг них.
- @gamedev_calendar - Канал информирующий о событиях игровой индустрии и крупных конференциях на территории СНГ.
- @vreality - Конфа VR энтузиастов
- @app2top - Сайт для разработчиков, издателей и маркетологов игр. Пишем о самом интересном в игровой индустрии.
- @app2top_gamedev_breaking - App2Top - про геймдев раньше всех
- @gamedev_events_MSK - все гемдев-мероприятия Москвы в одном месте
- @unity_news - Новости Unity, GameDev
- @gcconf - Game Construct Conference
- @gamedev_events - все гемдев-мероприятия СНГ и онлайна в одном месте
- @yangamesdevnews - Канал с новостями Яндекс Игр для игроделов

## Релокация

- @relogame - Все о релокации геймдева.
- @relogame_armenia - Relogame Armenia
- @relogame_turkey - Релокация геймдева и ИТ в Турцию
- @relogame_spain_chat - Relogame - Spain - Chat
- @relogame_la_chat - Relogame - Latin America - Chat
- @relogame_serbia - Русскоговорящий чат о жизни и переезде в Сербию.
- @relogame_georgia - Релокация в Грузию
- @relogame_uz - GGame - Uzbekistan
- @relogame_spain - Группа, которая поможет тебе переехать в солнечную Испанию
- @ReloGameFreelance - ReloGameFreelance - обсуждение

## Личные блоги (каналы)

- @black_temple - Геймдев Бомж. Меня зовут Никита Бельчак. Сооснователь геймдев студии BlackTemple
- @alladovhaliuk - С любовью о геймдеве и гейм-дизайне. Алла Довгалюк о нарративном гейм-дизайне.
- @diceanalytics - Случайные мысли и наблюдения продуктового аналитика в геймдеве.
- @gd_easy - Тайная комната для ценителей искусства создания игр и им сочувствующих.
- @vskobelev - Анализ дизайна актуальных игр. А ещё котики и моё исключительно субъективное мнение.
- @dev_game - Личный блог про геймдев, Unity разработку, AR/VR, компьютерную графику от эксперта.
- @kurilkagd - Записки гейм-дизайнера об устройстве игр, их роли в жизни человека и влиянии на культуру и общество.
- @backtracking - заметки об играх, геймдизайне и немного о процедурной генерации.
- @korovany - Авторский канал о геймдеве и не только — пишу о том, что интересно мне. Статьи, видео, книги, аналитика, мнения, новости и все такое.
- @progamedev_net - ProGameDev.net
- @TripleApr - Gamedev. Publishing/Marketing. Production.
- @pro_gameart - PRO Game Art
- @nuclearband - Пишу про геймдев и программирование в целом.
- @apanasik_jobless - Мысли Апанасика про IT, геймдев, игры и около отого.
- @gamedevils - Пишу про разработку игр, историю игр и геймдизайн. Делаю игры на Construct 3. Играю в LoL
- @tired_translator - Я перевожу игры с 2003 года, видела некоторое дерьмо и устала.

## Инди разработчики

- @gamedevtalk - Форум разработки игр
- @freegamedevkz - Group for free game dev from Kazakhstan. Сообщество вольных разработчиков игр из Казахстана.
- @gamedevhousetalks - Gamedev.House - это сообщество игровых разработчиков, в рамках которого проводятся митапы, хакатоны, фестивали и прочие мероприятия для всех представителей игровой индустрии.
- @Indikator_Chat - Уютный чат «Индикатор», креативного пространства для инди разработчиков.
- @IndikatorSPB_chat - Индикатор СПБ
- @indikator_innopolis_chat - Чат Индикатора в Иннополисе
- @indiepocalypse - Сообщество инди разработчиков
- @gamedev_chat_rus - Чат инди разработчиков для обсуждения идей, решения проблем, поиска единомышленников.
- @gamedevirk - Gamedev Irk
- @progamedev - Материалы о разработке, экспертные статьи и мои скромные мнения обо всём, что касается игр.
- https://t.me/+zVmWCE5cnj5kMDI6 - Инди-поддержка
- @NozomuGames - Делаю игры с необычными механиками, делюсь каждый день прогрессом разработки и своими подходами к созданию игр.
- @indiemakermain - индимейкер - геймдев здесь

## Unity

- @unity_flood - Филиал @unity3d_ru. Общение на темы Unity и разработка игр. Политика, оскорбления, гачи и срачи недопустимы в любом виде, если у вас РО - то, скорее всего, за это.
- @unity3d_ru - Общение о Unity3D. Оффтоп не приветствуется (идите в @unity_flood). Обсуждение идет в формате вопрос-ответ, будьте кратки и уважайте остальных участников.
- @ecschat - чат о разработках связанных с ECS, DOTS (DOD) в игровой индустрии
- @unity_architecture - Unity Architectural Astronauts
- @unity3d_usergroup - Группа о движке Unity, и прочих аспектах разработки компьютерных игр.
- @unity_cg - Unity CG, все что связано с графикой и Graphic Dev. Рендер, шейдеры, эффекты, текстуры, работа GPU, оптимизации и вычисления на видеокарте.
- @unitycg - Обратите внимание: Это Unity CG, все что связано с графикой
- @codeblog_csharp - Чат для .NET разработчиков и C# программистов.
- @sharphelp - Это сообщество программистов по языку С#

## Работа в геймдеве

- @gamedevjob - Работа в геймдеве
- @Gamedevjobs - Gamedev вакансии
- @agile_jobs - Публикуем вакансии и запросы на поиск работы - Agile, Scrum, Lean, XP, Kanban и т.д.
- @projects_jobs - Публикуем вакансии и запросы на поиск работы - управление проектами и рисками, PMBoK, PMP, бюджетированию и т.д.
- @products_jobs - Работа для людей, которые отвечают за успех продукта на рынке
- @User_Acquisition_Job - User Acquisition Jobs
- @seohr - SEO HR, digital-вакансии, офис и удалёнка
- @over100 - Здесь выкладываю вакансии из digital-отрасли с минимальной зарплатой 100 000 рублей.
- @digitalcv - Digital CV - резюме специалистов
- @devshr - Вакансии разработчиков
- @uiux_jobs - Публикуем вакансии и запросы на поиск работы по направлению дизайна UI, UX, Design Thinking, Style Guides, Mobile и т.д.
- @mobile_jobs - Публикуем вакансии и запросы на поиск работы по направлению iOS, Android, Xamarin и т.д.
- @qa_jobs - Вакансии в сфере QA.
- @devops_jobs - Публикуем вакансии и запросы на поиск работы по направлению DevOps & SRE. Обмен инсайдами и аналитикой на рынке труда DevOps & SRE.
- @javascript_jobs - JavaScript Jobs — чат для поиска работы и людей
- @nodejs_jobs - Публикуем вакансии и запросы на поиск работы для разработчиков на Node.js (back-end). 
- @devjobs - Game Development Jobs
- @ui_club - Чат для общения Web, UI, UX дизайнеров.
- @cg_worker - Размещаем вакансии и предложения работы в CG проектах.
- @unityjobs_pub Канал с вакансиями и фриланс-задачами под Unity.
- @animator_jobs - Доска объявлений для CG аниматоров, находящихся в поиске работы.
- @job_gamedev - Канал для поиска работы в геймдеве для 3д художников, программистов, QA тестировщиков, геймдизайнеров, 3д аниматоров, текстурщиков, моделлеров, скульпторов. 
- @editcghunter - Работа, вакансии и фриланс для реж. монтов и CG-спецов.
- @webgl_jobs - WebGL Jobs
- @gdjobs - Релокация и удаленка в GameDev.
- @ru_montage_pins - Вакансии и закреплённые сообщения с важной информацией
- @gamedevtalk - Форум разработки игр
- @gamedevjobtinder - Gamedev Job Finder
- @Freelance_CG_world - Доска объявлений CG. Публикуем вакансии и резюме. Размещение бесплатно
- @gdtalents - GDTalents – митапы и вакансии
- @cgfreelance - Канал поиска исполнителей в CG. Публикуем вакансии, задачи и резюме
- @CG_freelance - Вакансии и фриланс для CG Artists
- @DevJobs_Resume - Канал, в котором мы выложим ваше резюме для поиска работы как фриланс, так и для целый команды
- @forgamedev - Вакансии в Геймдеве. Собираем для вас вакансии, которых нет на работных сайтах
- @gamedev_vakansii - GameDev Jobs | Работа геймдеве
- @GameDev_and_Tech_vacancy - Jobs in GameDev & IT
- @gamedev_job - Работа в геймдеве
- @jobs_gamedev - Канал геймдев вакансий и резюме. Разработка игр; Фриланс от кодинга до 2D и 3D; Поиск специалистов в инди-команды;
- @gamedev_jobs - GameDev jobs
- @freelance_art - Работа вакансии для 2d 3d художников, аниматоров, иллюстраторов, дизайнеров, верстальщиков в сфере CG и Art растровая векторная графика интерьер графический дизайн перекладка blender 2д 3д макс фотошоп illustrator визитки behance Dribbble анимация digital
- @rabota_v_gamedeve - Работа в геймдеве (вакансии)
- @young_gamedev - Yo Gamedev - вакансии в геймдеве, CG
- @gamedevdialog - Форум обсуждения gamedev индустрии.
- @gamedev_unity_unreal_engine_jobs - Вакансии GameDev, Unity и Unreal Engine разработчиков
- @vakansii_gamedev - GameDev | Вакансии в геймедеве | CG Freelance
- @JobsforDevsandOps - Вакансии для ИТ-инженеров, админов, разработчиков.
- @qa_chillout_jobs - Канал с вакансиями для джунов QA и не только.
- @qa_work - Канал с вакансиями для тестировщиков.
- @jobs_designglory - Design Jobs
- @asorabota - Здесь публикуются вакансии и проектная работа по ASO.
- @weplaymakers - Крутые вакансии в GameDev в России и по всему миру

## ПО и ЯП

- @ohmy3dsmax - 3ds Max
- @cgchateek - 3ds max and friends
- @Maya3D - Основная направленность чата, общение на тему 3D.Оскорбления, флуд и реклама под запретом.
- @blender_ru - Blender_ru: вопрос-ответ
- @blender_cg_ru - Blender_CG: вопрос-ответ
- https://t.me/+DudVdJX7C3QyODRi - Болталка
- @blender_ru_talks - темы близкие к тематике сообщества. Blender, 2D и 3D графика, CG в целом.
- @zbrush_ru - Инфоканал по Zbrush. Помощь в освоении программы, обмен опытом и критика работ. За рекламой обращайтесь к админам. Нельзя: флуд, пиратские ссылки, стикеры.
- @Zbrush_renderu - ZBRUSH RENDER
- @zbrush_ru_talks - Zbrush_RU_talks
- @HoudiniGameTools - У нас про Houdini в геймдеве.
- https://telegram.me/joinchat/BbU_30E1zLoWMr04k4rwQQ - Oбщий чат о Houdini
- @VFX_chat - Группа любителей 3d графики. Для тех кто работает в 3d программах
- @SubstancePainterDesignerRU - Substance Painter & Designer
- @UnrealEngine4 - Unreal Engine
- @love2d_ru - Русскоязычное сообщество LÖVE
- @CoronaRu - Обсуждаем разработку на Solar2D & Corona Native
- @godot_engine - Godot Engine (Russian)
- @Godot_Engine_Offtop - Оффтоп чат для общения по теме геймдева и прочего айти. Общение по теме игровых движков, технологий, ПО и т.д.
- @DefoldEngine - RU сообщество Defold
- @unigine_ru - unigine_ru
- @renpyru - Данный чат посвящён игровому движку RenRy.
- @UE4Flood - Ламповая флудилочка чата Unreal Engine.
- @datsgamedev - флудилка Godot-чатика
- @prographon - Обсуждаем стандарты консорциума Khronos: OpenGL, OpenCL и пр., предлагаем различные работы, туторы, интересные материалы по этим стандартам (и помечаем их тегами).
- @rust_gamedev_ru - Rust GameDev — русскоговорящее сообщество
- @kubernetes_ru - Общаемся на темы, посвященные Kubernetes, конфигурации и возможностям.
- @rust_gamedev_ru_offtopic - Rust GameDev offtopic
- @webgl_ru - WebGL RU
- @gamedevforweb - GameDev for Web
- @PhysicITGameDev - Physic IT GameDev RU
- @UEVPHub - Virtual Production Hub - Unreal Engine
- https://t.me/+QbI8ipRghKwP8H5R - The Foundry Nuke
- @DaVinci_Resolve_ru - Здесь обсуждение работы в DaVinci Resolve.
- @BlackMagic_Fusion - BlackMagic Fusion
- @Redshift_render - Redshift Render
- @octanerender - Это чат по GPU RENDERS и, преимущественно, тематика должна касаться Octane, Redshift, Cycles и близлежащих к 3D тем.
- @yagamedev - Яндекс Игры — сообщество разработчиков

## Audio

- @zvuk_ne_nuzhen - Звук в Играх
- @game_audio_chat - Game Audio Evangelism Chat
- @wavinfo - Сообщество, в котором обсуждают звук, музыку и вообще.
- @lostinsound - Уютный Звук

## Жанры

- @hyper_casual - HYPER CASUAL by AZUR GAMES
- @hyper_casual_tech - HYPER CASUAL TECH
- @idler_clicker - Это чат про айдлеры и кликеры.
- @match_3 - Это чат про Match 3
- @mid_core - Это чат про мидкорные игры.

## Боты

- @AppSpy_Bot - Получи полный доступ к статистике игр и приложения из App Store и Google Play: Количество установок; Выручка за 3 месяца; Топ стран по доходам и скачиваниям.
- @SteamSpyBot - Получи полный доступ к статистики игр в Steam, анализируй конкурентов, выбирай перспективную нишу: Количество подписчиков и их динамику; Общую выручку игры и динамику за 3 месяца.
- @SteamPubBot - Будь в курсе новинок игровой индустрии. Отслеживай тренды рынка и релизы крупнейших студий в Steam. Бот покажет тебе все будущие релизы на всех платформах и уведомит, когда нужная тебе игра выйдет в Steam!

# Геймерские чаты

## Dota 2

- @dota_2_chat - DOTA 2 Чат
- @dota_hub - Чат | Dota 2
- @DotaChats - Чат | Dota 2
- @dota2junk_chat - Помойка чат Dota 2
- @dota2chat_Choogl - Dota 2 Chat
- @dota2_chat_2 - Обсуждение ставок. За спам и рекламу бан. Всё действия админов всегда обоснованы.
- @chglhfd2 - GLHF_Dota 2 Chat
- @ktvsdota - Чат Катавасии в Dota 2
- @dotatashkent - Дота2 Узбекистан - Ташкент
- @finargotchat - finargot - добрый чат
- @dota2uz - Группа для игроков, а так же любителей Dota 2 в Узбекистане.
- @Dota2Uzbekistan - Dota2Uzbekistan
- @deadinside7party - Dead inside community dota 2
- @taifbetchat - CYBER DOTA2 Чат

## Auto

- @crxdr2 - CarX Drift Racing 2
- @carxdr2free - [CarXDR2] Общий чат
- @carxdr3 - CarX Drift Racing 2 [NEW]
- @carxdriftracing2community - Torque Drift 2 Community
- @xdriveryt - XDriver YouTube × Chat
- @Car_parking_chat - CAR PARKING ЧАТ
- @sliders9 - Asphalt 9

## Genshin Impact

- @genshin_3 - Genshin Chat
- @traide_genshin - GENSHIN | ОБМЕН/ПРОДАЖА
- @buytradegensh - Обмен/Продажа | Аккаунты Genshin Impact
- @GenChatt - Genshin Impact | продажа | обмен | аккаунты
- @GenshinSellers - Genshin Impact I Покупка I Продажа
- @genshinrus - Неофициальное русскоязычное сообщество по игре Genshin Impact. Новости, общение, полезная информация, гайды, геймплей.
- @genshinchathonkaichat5 - Genshin Impact and Honkai Impact
- @floodpopku - Flood Genshin Impact
- @hdhsjzd - Flood Genshin Impact
- @flodQiQi - Flood Genshin Impact
- @genshinflood - Flood Genshin Impact
- @Flood_RP_Gensh - Flood Genshin Impact
- @paimon_chat - Паймон | Компаньон
- @genshix_chat - Геншин чатик
- @genshin_s - Genshin Impact - Обмен/Продажа аккаунтов
- @GenshinGachaChat - Добро пожаловать в оффициальный чат Genshin Gacha Simulator!
- @genshimpactchat - Genshin Chat
- https://t.me/joinchat/q69tOx2kbbA4MjVi - Genshin Impact

## CS GO

- @CSGOChats - Чат | CS:GO
- @csgohouse_chat - CS:GO HOUSE | FUN CHAT
- @csgofuture - CSGOFUTURE
- @gc_skins_chat - ОБСУЖДЕНИЕ GC.SKINS
- @csgobetachat - Обсуждение Counter-Strike: Global Offensive на русском языке.
- @aitucsgo - CS:GO AITU

## PUBG

- @pubg_mobile_chat_ru - PUBG MOBILE
- @PUBG_MOBILE_CHAT_RUS_UZB - PUBG MOBILE CHAT
- @pubgmruofficial - Чат PUBG MOBILE
- @cosmopm - Cosmo [Chat]
- @Team_Chat88 - Читы на Pubg mobile и другие игры.
- @shat_obshchenia - Официальный ЧАТ ДЛЯ ПАБГЕРОВ
- @MetroRoyalChat - детдом Задрота MetroRoyale
- @pubgkchr - 𝙿𝚄𝙱𝙶 𝙺𝙲𝙷𝚁
- https://t.me/joinchat/Rqv1OXT0Y829bdZN - PUBG
- @xlpubg - PUBG XL-Games.ru
- @fizikstorechat - Лучший чат снг по продажам и покупкам аккаунтов в PUBGM

## NFT Game

- @nftgamerss - Здесь мы обсуждаем NFT-проекты, арты и многое другое по теме невзаимозаменяемых токенов
- @NFTgamebots - Боты для NFT игр. Готовые и под заказ.
- @Mobox_RUS - Mobox Россия (mobox.io )
- @GodsUnchained_rus - Gods Unchained is a free-to-play trading card game where players compete in epic duels using fantasy cards.
- @gods_unchained_rus - GODS UNCHAINED
- @godsunchained_russian - Общаемся по игре Gods Unchained. Обсуждаем последние новости и токен игры.
- @gods_russia - Gods Unchained - бесплатная онлайн игра, в которой игроки соревнуются в эпических поединках, используя фантастические карты. Игра создана для того, чтобы коренным образом изменить принцип работы игр, используя сеть Ethereum, чтобы предоставить игрокам нас
- @steem_monsters_ru - Русскоязычный чат ККИ SPLINTERLANDS (ex-STEEM MONSTERS)
- @CanaBoyz_RU_Chat - CanaBoyz p2e NFT game [СНГ]
- @nftgame_chat - Здесь мы обсуждаем все новости Play-to-earn / NFT проектов и делимся интересными событиями
- @evergaming_chat_ru - EveGaming — это экосистема для любителей игр. Играй и зарабатывай, получай NFT и криптовалюту!
- @NFTSquidGameRU - Squid Game #Players Чат
- @nftpanda_ru - Чат посвящён криптоигре NFT Panda: WoF.
- @CryptoGamingPool - Первое Русскоязычное Криптогейминг Сообщество & Медиа!
- @aavegotchi_rus - Чат рускоязычного сообщества Aavegotchi
- @starsharks_sss_russian - StarSharks is a NFT-GameFi ecosystem based on the BNB chain developed by game players, governance committees, and game developers.
- @qchain_qdt - Обсуждаем NFT и P2E игры
- @xmeta_russian - X-Metaverse Russia chat
- @thetanarenaru - Thetan Arena Russian
- @seedifyrussia - Seedify Russian Speakers
- @continuumworld_ru - Continuum World Ru
- @NineChronicles_RU_CGP - Чат по криптоигре Nine Chronicles.
- @NineChroniclesRus - Русскоязычный чат по игре Nine Chronicles
- @NineChronicles_RU - Чат посвящён криптоигре Nine Chronicles.
- @heroverse_ru - HeroVerse RU - русскоязычный чат
- @seascapenetworkru - Seascape Network (Official Russian Community Group)
- @galaxyblitzru - Galaxy Blitz is a Play-To-Earn combat strategy NFT game where you lead the descendants of humanity to forge a new empire
- @minesofdalarnia_ru - Чат посвящён криптоигре Mines of Dalarnia.
- @MinesOfDalarniaRU - Бесплатная экшн-игра с уникальным блокчейн-рынком недвижимости
- @minesofdalarnia_token - Mines of Dalarnia | Free Token | RU
- @kingspeedrussian - Официальное русскоязычное комьюнити KingSpeed
- @play2egames - Чат посвящён разбору как новых, так и существующих криптоигровых проектов. Создан русскоязычным криптогейминг сообществом.
- @digitalnft - Один из пионеров в ру сегменте чат посвещённы NFT и крипто-играм, созданный в феврале 2021 года.
- @warriders_ru - Чат по криптоигре War Riders.
- @widilandrussia - WidiLand Russia Community
- @galaxyonline_ru - Чат игры ГАЛАКТИКА ОНЛАЙН.
- @metoshi_rus - Развлекательная платформа: GameFi / Биржа / Комиксы / NFT
- @celestialrussia - Celestial Russia #Metaverse starwar, blockchain world first Gamefi+Socialfi+Nftswap.
- @crosstheagesru - Официальная русскоязычная группа, посвященная новостям Cross The Ages
- @mechmaster_russia - Mech Master - это пошаговая ролевая игра в жанре стратегии NFT, в которой вы используете технологии будущего для обустройства своей территории и защиты родины.
- @gamefitalks - Добро пожаловать в чат по GameFi. Обсуждаем тут разработку NFT-игр.
- @faraland_ru_chat - Публичный чат канала Faraland (RU). Faraland - это ролевая стратегия для Android и iOS, разработанная на блокчейне (в настоящее время BSC - Binance Smart Chain) разработчиком MoonKnight Labs.
- @shibafriend_russia - ShibafriendNFT Russian
- @platofarmrussiancommunity - Plato Farm — это метавселенная, использующая технологию блокчейна для имитации эволюции человеческой цивилизации. Благодаря упорному труду игроки могут превратить дикую местность в богатый и современный дом
- @angelicrussian - Angelic — это повествовательная RPG-стратегия, основанная на блокчейне.
- @farmingtales_ru - Чат посвящён криптоигре FarmingTales.
- @rcupgame - Rust Cup Game — это гоночное ралли с непрямым управлением, где игроки соревнуются на трассах разной сложности, выигрывая токены EVER.
- @nftandgames - В данном чате Вы найдете самые новые новости и обсуждения по NFT, FREE to PLAY, PLAY to EARN играх, узнаете как можно без вложений или с минимальными вложениями зарабатывать играя! И конечно же откроете для себя мир криптовалюты!
- @alienworlds_ru - Чат по криптоигре Alien Worlds. Создан первым русскоязычным криптогейминг сообществом
- @colizeum_russia - The Official Colizeum.com Russia Community Group
- @profitcryptogameschat - Profit Crypto Games Chat
- @genopetssng - Genopets — это бесплатная игра NFT с функцией «ходи и зарабатывай» на Солане, которая побуждает игроков оставаться активными и зарабатывать бесплатно.
- @alteration_cis - Alteration — это Play-To-Earn NFT игра.
- @tiny_world_ru - Чат по криптоигре Tiny World.
- @tinyworld_ru - Tiny World RU
- @pvurussia - Дружное сообщество игры Plants vs Undead для русскоязычного комьюнити.
- @rcupgame - это гоночное ралли с непрямым управлением, где игроки соревнуются на трассах разной сложности, выигрывая токены EVER.
- @warenachat_russia - Warena Chat - Россия
- @plantvsundead_rus - Чат посвящён криптоигре Plant vs Undead.

## League of Legends

- @lolc_chat - League of Legends Community Chat
- @WildRiftChatt - Hawlax chat | Wild Rift
- @Chat_WRF - WR Family | Chat
- @wildriftchatru - Wild Rift Chat
- @wildriftch - Сообщество Вайлд Рифт
- @gilde_c_k - Чат Игроков кланов Cyber Killers
- @the_summoners_rift - Телеграм канал (чат) League of Legends (Лига Легенд), для обсуждения игры и не только.
- @league_legends_chat - Добро пожаловать в телеграм чат посвященный игре Лига Легенд!
- @chat_league_of_legends - Чат создан для помощи в поиске напарника в League Of Legends, а также для простого общения
- @mrkchat - МРК чат по League of legends
- https://t.me/joinchat/eKUU_QWvA2UxOTli - LoL

## Mobile Legends

- @ML4PDA21 - Mobile Legends: 4PDA
- @mlbbtalk - Чат | Mobile Legends: Bang Bang
- @PEGASUS_MLBB - PEGASUS | MLBB | Mobile Legends
- @mlbbchatgroup - Чат | Mobile Legends: Bang Bang
- @mlbbgamesng - MLBBGAME - KG

## Pokemon GO

- @pokemon_go_raid - Pokemon GO Raid - Покемон Го Рейды
- @chat_pokemon_go - Флудильня
- @RaidKhab - Рейды Хабаровск Pokemon Go
- @PokemonGoKhv - Тут собраны игроки Хабаровска по игре Pokemon Go.
- @pokemongochatspb - PokemonGoChat
- @pokemongocheating - “AirPokeGo” Штаб Читеров
- @pokemontmn - Клуб игроков в Pokemon Go в Тюмени.
- @pogobalashikha - Основной чат г.Балашихи, где есть всё о Pokemon Go.
- @pokemontcg_chat - Чат Московской Лиги Покемон ККИ
- @pokgopoltava - Ты в группе полтавских тренеров. Здесь обогреют, накормят и напоят. Будь спокоен и вежлив. Задавай любые вопросы. Не забывай получать удовольствие от любимой игры и общения.
- @poketver - Тверской чат игроков Pokemon go.
- @pokemongokyivraid - Pokémon Go Kyiv Рейд Правый Берег
- @pokemongokyiv - Pokémon Go Kyiv Межфрак
- @pogokem_flood - PoGoKem-Flood
- @PoGoKemYugniyFPK - [FLOOD]PoGoKem.Южный/ФПК
- @pogokem_distant - Группа для организации и проведения дистанционных рейдов.
- @altaypokemongo - Pokémon GO Altay
- @pogovd - Pokemon Go! Рейды Волгодонск
- @spbpokego - КФ СПб Poke go
- @pgsevxl - Клуб покемонщиков Севаст0п0ля
- @apokeraid - Канал поклонников игры Pokemon GO в г. Александров.
- @pokemonodintsovo - OPoGo Pokemon Go Одинцово
- @pogoptg - Чат Pokemon Go в Пятигорске.
- @KMVpogo - Старая группа покемонщиков из Минеральных Вод.
- @pokeYess - Pokemon Go | Ессентуки
- @drunkpikachu - Синий Пикачу
- @pogood_poskot - Цель группы - помочь игрокам Pokemon Go организовывать совместные рейды
- @reunovraids - Данная группа создана для координации рейдов в Реутове и Новокосино.
- @pogodon - Pokemon Go in Donetsk
- @pokemonnvkz - Группа для общения игроков Pokemon GO в г. Новокузнецк
- @pokemonmoscow - Pokémon GO Moscow
- @pokekvartal - Pokemon Kazan кварталы

## Minecraft

- @Minecraft_Choogl - Чат Майнкрафтеров
- @minecraftapp_chat - Чат по Майнкрафту. Спрашивайте свои вопросы, присылайте на оценку постройки.
- @mineproject312 - Телеграмм чат Minecraft project!
- @maynckrafters_group - Community Майнкрафтеры
- @evercraft_ru - Официальная русскоязычная группа плагина EverCraft для Minecraft
- @igra_minecraft_ru - Minecraft Ps4

## Rush Royale

- @rushroyaleru - Неофициальная группа RushRoyale от портала http://rushroyaledecks.net 
- @rushroyale - Rush Royale (Rus)
- @we_rushroyale - Мы мирные зайки
- @kazrusro - Казань_RushRoyale
- @dragonsliar - Dragons Liar, Беседа кланового чата.

## Roblox

- @Robloxxxox - Ты попал в большую группу по обмену в роблокс
- @adopt_wenssly - adopt me trade
- @Trademm2adop - адопт и мм2 трейды
- @adoptmesel - ADOPT ME TRADE
- @RobloxCorruptov - Adopt Me|Trades
- @tradeadoptmemm2roblox - Чᴀᴛ дᴧя ᴏбʍᴇнᴏʙ Adᴏᴩᴛ Mᴇ || Murdᴇr Mysᴛᴇry 2
- @mm2_adoptme_trades - Чат для общения | Зона треидов
- @Adopt_me_too - Adopt Me | Murder Mystery 2 | ROBLOX ЧАТ
- @roblox_litemod - Привет! Этот чат не только про этот мод

## Fallout 76

- @Fallout76Chat - Игровой Чат онлайн игры #Fallout76
- @fallout76_ru - Fallout 76 русскоязычный чат
- @Fallout76_Russia_chat - Игровой чат онлайн игры #Fallout76 для всех платформ.

## War Thunder

- @WarThunderRuChat - War Thunder Chat RU
- @WarThunderBlog - War Thunder Blog [Chat]

## Supercell Games

- @chatposigion - Brawl Stars Postigion | Chat
- @ESG_BrawlStars - Турниры по Brawl Stars от Everscale Gaming!
- @brawlstars_chat_ru - Brawl Stars Chat Ru
- @lbo_chat - Lucky Brawl Originals — чат
- @bscoolchat - BSC Chat | Brawl Stars Cool Chat
- @chatclashofclans - Глобальный чат по Clash of Clans Клэш
- @chat_clashofclans - Приветствуем тебя в чате, посвященном игре Clash of Clans Здесь можешь найти себе клан, делать наборы в свой клан, обсудить последние новости клеша и просто пообщаться.
- @clashofclans_prodaza - Clash of Clans Продажа/Общение 
- @ESG_ClashRoyale - ESG Clash Royale
- @shoot_royale - Welcome to our friendly and active Supercell games chat
- @boltalkahayday - общение Hay Day
- @haydaymurachat - Добро пожаловать в чат HayDay Communication
- @supercell_prodazha_ru - В этом чате вы можете продавать, покупать, или обмениваться аккаунтами игр SUPERCELL
- @pioneersclan - Clash of Clans ПИОНЕРЫ
- @grandclashroyale - Clash Royale чат
- @BrawlFree_Ru - Дᴏбᴩᴏ ᴨᴏжᴀᴧᴏʙᴀᴛь ʙ нᴀɯ дᴩужᴇᴧюбный чᴀᴛ ᴨᴏ Brᴀwl Sᴛᴀrs!
- @clashroyalecantban - ClashRoyale[ICE LINE]
- https://t.me/+9v1x_LdauH04Mjg6 - Supercell
- @royale_esports_chat - Royale Esports
- @supercellpostigionchat - Supercell Postigion Chat

## Raid Shadow

- @raidShadowLegend - Raid Shadow Legends
- @Verdan_raid_chat - Играем в Raid Shadow Legends!
- @inquisitionraid - RAID Shadow Legend
- @highlanderchat - HIGHLANDER CHAT RAID

## Slot Games

- @Slotgames_chat - SlotGames чат КАЗИНО
- @ggpokerok_chat - Чат для общения игроков (18+) о покере
- @affgambler_chat - Чат игроков казино онлайн для обсуждения новостей, событий, бонусов, казино, слотов, решения вопросов и т.д.
- @casinopluha - Группа для любителей азарта :слотов и игровых автоматов, покера, рулеток, и казино в целом ).
- @danludan_chat3 - Чат для любителей азарта : слотов и игровых автоматов покера рулеток и казино в целом )
- @luckyslots - Lucky chat
- @casino_bonus_goodspin - Казино онлайн топ бонусы и турниры в онлайн казино! Актуальные рабочие зеркала.
- @poker_ru_chat - Место, где общаются покеристы!
- @winamax_ru - Winamax poker Russian - чат

## Xbox

- @family_xbox - Тайная комната иксбоксеров и одного сонибоя 
- @xboxruchat - Xbox чат
- @xbox_community_chat - XBOX Community Chat | Чат ИксБокс
- @xbox_store_arg - Xbox Store Argentina — Chat

## Play Station

- @pspxr - PSPx.RU чат
- @playstation_squad - Чат создан для конкурсов и обсуждения всего что связано с PlayStation и игровой индустрией
- @pIaystation_squad - Чат для обсуждения всего что связано с PlayStation и игровой индустрией, и поп культурой И просто для общения.
- @playstation4teammates - Поиск тиммейтов Apex Legends, COD Warzone и других для платформы PS4!
- @ps4nru_gr - PSXN - чат про PlayStation и все остальное - PS5, PS4, Pro, PS VR, PS Plus
- @ps5rus - Уютный чатик о Playstation 5.

## Консоли

- @pronintendo_ru - Chat PRO Nintendo
- @repairps - Группа для сервисов и мастеров по ремонту игровых консолей
- @pastgengames - Ретрогейминг, старые игры, консоли, компьютеры (dendy, sega...)
- @dmg_group - Официальная группа YouTube канала "DMG Group". Добро пожаловать! DMG (Dot Matrix Game; рус.: игра на точечной матрице) - кодовое имя консоли Game Boy Original.

## World of Tanks

- @New_Just_Jokes_Chat - New Just Jokes Chat (WoT Blits)
- @klan_blohi - World of Tanks Blitz
- @aleks_aktobe - wotblitz
- @ivan12381 - Цинк Blitz
- @bletzchatik - BlItZ/ChATiK
- @me1w1n_chat - WoT Blitz | Продажа | Покупка | Аккаунты | Золото

## FIFA 22

- @fifasocceru - Чат для опытных игроков в Fifa 22
- @fifa_mobile17 - FIFA MOBILE 22 RU
- @naix_g - NAIXGAME - FIFA 22 MOBILE Chat

## Настольные игры

- @aitu_board_game_club - “Critical Success” – Клуб настольных игр AITU
- @magicworldc - Игровой клуб настольных игр в Киеве
- @bgcspb - Чат коммьюнити в SPb. Сборы, фото, обмен впечатлениями, только околонастольные темы.
- @lrs_dnd - Данный чат создан командой LivingRoomStudio (LRS) и посвящён обсуждению её творчества и настольной ролевой игре (НРИ) Dungeons&Dragons.
- @chat_go_weiqi - Чат клуба Го

## Cyber Games

- @cyberxufa_chat - Киберклуб CyberX Community Уфа Гостиный двор. Общение, ваши вопросы и ответы, резерв PC.
- @esport_poltava - Полтавський КІБЕРСПОРТ
- @freeton_sports - Объединение Спортсменов Everscale Sport Community
- @uesf_fludilka - UESF_FLUDILKA — офіційний канал Федерації кіберспорту України, де можна вільно спілкуватися з іншими гравцями.
- @ez_k4tka_chat - Мир киберспорта. Чат
- @gameri_vse - Геймеры, Киберспорт. Чат

## Чаты различных игр

- @discussionCODM - Общение | Call of Duty: Mobile @wotexpress_chat - Это чат WE основного канала исключительно в Telegram для обсуждения WOT и иных проектов Wargaming. @free_fire_fre_fire - Free Fire Продажа акаунтов @KINGKITSAXIE - Axie Infinity King Kits Комьюнити @PoeRu - Path of Exile | СНГ PoE @spaceflight_simulator_chat - Spaceflight Simulator UA\RU
- @crocodilegame - Чат для игры в Крокодил
- @freefirechattop - Фри Фаер Чат
- @MafiaWar - Чат игры Mafia War
- @Quizarium - Нон-стоп чат по игре Quizarium
- @HearthChat - Чат Hearthstone
- @GidvelikiyChat - Великий Султан фан чат. Chat Game Of Sultans
- @BFGpostigion - BFG Postigion | Chat
- @AwakenChaos - Awaken Chaos Era
- @krazzworks - Чат про World of Warcraft
- @sw_raid_groop - Summoners war Беседа
- @forbiddenbs - Forbidden | BS
- @baza_lm - БАЗА Lords Mobile
- @albion_free - Albion Online
- @aov_chat - Чат по игре Arena of Valor. Лучшая MOBA 5v5
- @mythwarss - Чат помощи и обсуждения по игре MythWars.
- @pocketcombats_chat - Pocket Combats RPG | текстовая ролевая игра
- @thesandboxgameru - The Sandbox — новости и обсуждения проекта
- @pes_mobile_4pda - Игровой канал по игре Єfootball / Pro Evolution Soccer.
- @stalkerbaza - Ролевая игра #База_сталкеров
- @chctournament - Основной чат турнира по Custom Hero Clash
- @ESG_FF - Турниры по Free Fire от Everscale Gaming!
- @gmodchat - Крупнейший русскоязычный чат по гаррис моду
- @anomalyrf - Официальный русскоязычный чат знаменитого мода Anomaly на Call of Chernobyl
- @esg_cod - Турниры по COD Mobile от Everscale Gaming!
- @anomalychat - Чат канала S.T.A.L.K.E.R. Anomaly
- @EnlistedBlog - Чат об игре Enlisted.
- @gtaonline_chat - У нас вы можете обсудить последние новости из GTA и Red Dead Redemption 2, найти новых друзей и банду!
- @hokageultimatestorm - Hokage Ultimate Storm Русскоязычное сообщество
- @kedalion2 - STORMFALL RISE OF BALUR Доминионы/КЕДАЛИОН
- @bleach_3d_global_fix - Чатик про игру блич 3Д мобайл
- @starcraftchat -  StarCraft-chat
- @superworldboxq - Русскоязычный чат по игре Super WorldBox
- @newsfront_standoff2 - Чат по Standoff 2, общение, обсуждение новостей
- @ekbcheapshot - А это неофициальный чат для жителей и гостей Екатеринбурга играющих в Cheapshot.
- @fermasosedichat - FermaSosedi chat
- @transformice_chat_ru - Неофициальный русскоязычный чат для любителей игры Transformice, Transformice Adventures и не только.
- @mk11_ru - Общаемся на темы, посвященные игре Mortal Kombat 11.
- @turnbasedtactics - Группа про пошаговые тактические игры (Turn-based tactics (TBT), tactical turn-based (TTB), TRPG, squad tactics).
- @goldwarspear - WarSpear Чёрный Рынок
- @avatariaofficialcontest - Официальная группа проведения конкурсов игры Аватария 101xp
- @thedivision2ru - Tom Clancy’s The Division 2. Это чат для обсуждений, поиска игроков и пати.
- @witcherchameleon - Хамелеон. The Witcher: Monster Slayer
- @thewitchermonsterslayer_tyumen - The Witcher: Monster Slayer/Тюмень
- @osuruchat - OSU! Russian Chat
- @ddchat - Чат игроков в Darkest Dungeon.
- @mtgchat - Чат для новичков и профессиональных игроков Magic The Gathering, занимающихся поиском игровой мудрости.
- @kocmoc_halll - Русскоговорящее комьюнити по Destiny на Xbox one.
- @warcraft_meme_chat - World of Warcraft Meme | Chat
- @magnumquest_warrior - Группа для помощи новичкам и приёма заявок в одну из топ гильдий ᎳᎪᏒᏒᎥᎾᏒ, игра Magnum Quest.
- @rp_gw - Чат GameWorld. GameWorld - самый зрелый и взрослый русскоязычный проект GTA San Andreas: Multiplayer с Role-Play режимом.
- @polytopia_ru - Русскоговорящая группа по игре Polytopia
- @hideonlinechat - Чатик про Hide Online и не только
- @platinatri - страница созданная для участников игры DOWN OF TITAN . делитесь ссылками , эмоциями , своими достижениями в игре.
- @blackmoba - Игра mobitva ONLINE - бесплатная онлайн игра в стиле MMORPG для мобильных телефонов.
- @sniper3d_kurilkaa - Sniper3D Курилка INTERNATIONAL
- @aimasterhl - AIMaster Half-Life
- @msfs2020chat - Microsoft Flight Simulator 2020 Chat
- @mtgarenaru - MTG Arena ЧАТ (Бесплатная компьютерная коллекционная карточная игра, разработанная и изданная Wizards of the Coast.)
- https://t.me/joinchat/FUBM1BlimpNEHkvGp_8JmQ - Tom Clancy's Rainbow Six: Siege
- https://t.me/+8FueSQIBrHkzMTI6 - VALORANT
- https://t.me/joinchat/2mi5GJe7ZvJjOTNi - Apex Legends
- https://t.me/joinchat/MiJVPdMtXMpkMjIy - Rocket League
- https://t.me/+pV89K2wLNTA0ZTRi - OSU
- https://t.me/+Q5k4CbEKF_c0NzBi - GTA
- @ecogameruchat - Группа для общения и обсуждения тем связанных с игрой Eco от StrangeLoopGames.
- @package_rus - Официальный чат игры Package
- @afk_inside_chat - Неофициальное фан-сообщество по игре AFK arena

## Разное

- @GameChatThe - Это же Game Чат!
- @rpgcru - RPGC Russian Chat
- @ygchat - Глобальный чат пользователей форума https://yougame.biz/
- @tgamer_bf2 - T~GAMER | BF2
- @NorlinGamesChat - Norlin Games chat
- @AnjiChat - чат по проектам от Анжи
- @barren_burthen_chat - BARREN BURTHEN
- @luna_community - Чатик про Луну
- @chatprepoda - Чат для подписчиков Препода
- @rybanewschat - Балык NEWSwws
- @ru2chvg - Видеоигры
- @igrapalata - Чат игр, обсуждения лишь в рамках и относительно игры. 
- @AWAKEN_NEWS_PUBLIC - AWAKEN
- @sexwithminion - присутствуют комьюнити : Dota 2, CS: GO, Minecraft, Genshin Impact, Valorant, The Witcher, Dying Light, Grand Theft Auto, Brawl Stars, League of Legends.
- @unu_games - Чат для различных игр. Помогает расслабиться, отдохнуть и забыться.
- @roleplay_chat - У нас вы найдете интересные или нет, ролевые игры по своему вкусу.
- @ProGames_Chat - Новости и обсуждения видеоигр!
- @langamers - Привет! Это чат http://langame.ru , где мы обсуждаем последние статьи на нашем канале компьютерные клубы и всё, что с ними связано.
- @bnl_gamers - BNL_gamers
- @blind_games - В данной группе Вы можете обсуждать игры, помогать или попросить помощи у других пользователей, найти новых друзей и знакомых для совместных игр.
- @holygameschat - Чат для общения на интеллектуальные темы. А так же посвящён разработке игр.
- @greatgamerchat - Все об индустрии игр и кино. Общайтесь и обсуждайте материалы сайта и канала, придумывайте интересные темы, знакомьтесь и находите новых друзей.
- @linuxgamingru - Русскоязычный чат, посвящённый играм на различных дистрибутивах Linux. Является филиалом чата по дистру Arch Linux, однако им не ограничивается.
- @silentflow_gazebochat - Беседка для Всех кому хоть Немного нравятся игры :)
- @rudiscord - Играем в разные игры на ПК на PlayStation и XBOX.
- @igornaya - Игры и боты
- @fatplay - FAT PLAY Чат
- @la2chat - LineageTWO.ru - Чат
- @itsmyshelerbitch - Очередной чат очередного стримлера, да да я
- @he_sus - Хесус или Джесус?
- @igameon - GAME ON!
- @nightblackwolfgamerchat - Это Геймерский чат Night black wolf Он играет и стримит на ютубе и твитче. Здесь можете обсуждать различные игры, а так же множество других тем, которые не смущают людей
- @norlingameschat - Norlin Games chat
- @kbtucybersport - Геймерское коммьюнити KBTU
- @gmodfangroup - GMod-Fan Inc. Chat
- @zelwingames - marketpalce for games
- @multichat - Обсуждаем игры, приставки, события и иногда разыгрываем что-нибудь.
- @plaggame - Играем в сессионки и собираем лобби в стратегии.
- @keypc_chat - Ключник чат
- @wlg_chat - Wagon League Gaming.
